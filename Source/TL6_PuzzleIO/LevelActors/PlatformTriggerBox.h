// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "PlatformTriggerBox.generated.h"

/**
 * A trigger which signals one or more platforms to move.
 */
UCLASS()
class TL6_PUZZLEIO_API APlatformTriggerBox : public ATriggerBox
{
	GENERATED_BODY()
	
public:
	
	APlatformTriggerBox();

protected:
	
	void BeginPlay() override;

	void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere)
	// Platforms which start moving when this actor is triggered
	TSet<class AMovingPlatform*> PlatformsToTrigger;

	UPROPERTY(EditAnywhere)
	// Platforms which stop moving when this actor is triggered
	TSet<class AMovingPlatform*> PlatformsToInhibit;

	UFUNCTION()
	void Activate(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION()
	void Deactivate(AActor* OverlappedActor, AActor* OtherActor);

};

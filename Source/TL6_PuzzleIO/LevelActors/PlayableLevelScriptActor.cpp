// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayableLevelScriptActor.h"
#include "Core/TL6_PuzzleIOGameInstance.h"
#include "TL6MenuWidget.h"


void APlayableLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();
	
	using UGI = UTL6_PuzzleIOGameInstance;
	UGI* GameInstance = Cast<UGI>(GetGameInstance());
	InGameMenuWidget = GameInstance->LoadMenuWidget(GameInstance->GetInGameMenuWidgetClass());
}

void APlayableLevelScriptActor::LoadMenuWidget() const
{
	if (InGameMenuWidget && InGameMenuWidget->IsValidLowLevelFast())
	{
		InGameMenuWidget->IsWidgetOnScreen()? InGameMenuWidget->HideWidget() : InGameMenuWidget->ShowWidget();
	}
}

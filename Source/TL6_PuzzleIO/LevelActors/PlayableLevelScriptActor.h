// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "PlayableLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class TL6_PUZZLEIO_API APlayableLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

protected:
	void BeginPlay() override;

	// Bound to a Keyboard event in the Level Blueprint
	UFUNCTION(BlueprintCallable)
	void LoadMenuWidget() const;
	
	class UTL6MenuWidget* InGameMenuWidget;
};

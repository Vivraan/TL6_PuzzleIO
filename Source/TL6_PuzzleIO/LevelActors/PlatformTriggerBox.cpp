// Fill out your copyright notice in the Description page of Project Settings.

#include "PlatformTriggerBox.h"
#include "MovingPlatform.h"
#include "Core/TL6_PuzzleIO.h"

#include "Components/BoxComponent.h"
#include "Components/BillboardComponent.h"

#define DO_CHECK 1

APlatformTriggerBox::APlatformTriggerBox()
{
	PrimaryActorTick.bCanEverTick = true;
}

void APlatformTriggerBox::BeginPlay()
{
	Super::BeginPlay();

	OnActorBeginOverlap.AddDynamic(this, &APlatformTriggerBox::Activate);
	OnActorEndOverlap.AddDynamic(this, &APlatformTriggerBox::Deactivate);

	// Hide the collision box and trigger sprite in-game
	GetCollisionComponent()->SetHiddenInGame(true);
	// The trigger sprite will either be the original component or another object made from thin air
	//GetSpriteComponent()->SetHiddenInGame(true);

	// Ideally no platform should be both set to be triggered and inhibited
	requireMsg(PlatformsToTrigger.Intersect(PlatformsToInhibit).Num() == 0,
		TEXT("Some platforms being triggered are also being inhibited! Exiting!"));

}

void APlatformTriggerBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlatformTriggerBox::Activate(AActor* OverlappedActor, AActor* OtherActor)
{
	// For each registered platform, vary count by the number of enabling influences
	for (AMovingPlatform* Platform : PlatformsToTrigger)
	{
		Platform->IncrementTriggerCount();
	}
	
	// For each registered platform, vary count by the number of inhibiting influences
	for (AMovingPlatform* Platform : PlatformsToInhibit)
	{
		Platform->DecrementTriggerCount();
	}
}

void APlatformTriggerBox::Deactivate(AActor* OverlappedActor, AActor* OtherActor)
{
	// For each registered platform, vary count by the number of enabling influences
	for (AMovingPlatform* Platform : PlatformsToTrigger)
	{
		Platform->DecrementTriggerCount();
	}
	
	// For each registered platform, vary count by the number of inhibiting influences
	for (AMovingPlatform* Platform : PlatformsToInhibit)
	{
		Platform->IncrementTriggerCount();
	}
}

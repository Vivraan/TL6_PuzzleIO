// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuLevelScriptActor.h"
#include "Core/TL6_PuzzleIOGameInstance.h"
#include "TL6MainMenuWidget.h"

void AMainMenuLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	using UGI = UTL6_PuzzleIOGameInstance;
	UGI* GameInstance = Cast<UGI>(GetGameInstance());

	if (UTL6MenuWidget* MainMenuWidget = GameInstance->LoadMenuWidget(GameInstance->GetMainMenuWidgetClass()))
	{
		MainMenuWidget->ShowWidget();
	}
}

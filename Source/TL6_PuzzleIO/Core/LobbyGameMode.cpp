// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyGameMode.h"
#include "Core/TL6_PuzzleIO.h"

#include "Engine/World.h"

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	if (++NumPlayers >= MAX_NUM_PLAYERS)
	{
		if (GetWorld())
		{
			bUseSeamlessTravel = true;
			GetWorld()->ServerTravel(TL6_PuzzleIOStatics::PlayableLevel + TEXT("?listen"));
		}
	}
}

void ALobbyGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	if (--NumPlayers <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Where'd they all go?!"));
	}
}

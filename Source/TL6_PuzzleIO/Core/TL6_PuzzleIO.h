// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

// macros for TL6_PuzzleIO_Statics::Require, to ease off having to put in a few parameters
#define require(bCondition) TL6_PuzzleIOStatics::Require(GetWorld(), (bCondition), TEXT(""))

// macros for TL6_PuzzleIO_Statics::Require, to ease off having to put in a few parameters
#define requireMsg(bCondition, Context) TL6_PuzzleIOStatics::Require(GetWorld(), (bCondition), (Context))


struct TL6_PuzzleIOStatics final
{
	TL6_PuzzleIOStatics() = delete;

	// The three levels currently used in the game.
	static const FString MainMenuLevel;
	static const FString LobbyLevel;
	static const FString PlayableLevel;

	// A goddarn assert with some real world value
	static void Require(const class UWorld* World, bool bCondition, const FString& ContextString);

	// A hacky IPv4 Address validator. Accepts "localhost": doing so modifies out parameter. 
	// Avoids including network module (for now).
	static bool MakeValidIPv4Address(FString& OutAddress);

	// Prints to log, and if possible, to the screen too!
	static void PrintToScreenAndLogTemp(FString LogMessage, FColor LogColor = FColor::Yellow);
};

/** A struct for holding the values from search results as formatted strings. Useful for debugging and lowering coupling. */
struct FOnlineSessionSearchResultStrings
{
	FString SessionIdStr;
	FString PingInMs;
	FString OwningUserName;
	FString OpenPublicConnectionsRatio;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSessionInterface.h"
#include "TL6MenuInterface.h"

#include "MenuSystem/MenuInterface.h"
#include "TL6_PuzzleIOGameInstance.generated.h"


class UTL6MenuWidget;
/**
 * Defines some console commands to be used for initiating a game.
 */
UCLASS(Config=TL6Config)
class TL6_PUZZLEIO_API UTL6_PuzzleIOGameInstance : public UGameInstance, public ITL6MenuInterface
{
	GENERATED_BODY()

public:

	// Initialise GameInstance properties.
	virtual void Init() override;

	void InitializeOnlineSessionInterface();

	// Creates a Menu widget from the supplied UClass.
	UTL6MenuWidget* LoadMenuWidget(TSubclassOf<UTL6MenuWidget> MenuWidgetClass);

/// IMenuInterface
	// Returns local player to the lobby level.
	virtual void GotoMainMenuLevel() final override;
	
	UFUNCTION(Exec)
	// Console command to host a game, also used by the UI.
	virtual void HostNamed(const FString& ServerName) final override;

	UFUNCTION(Exec)
	// Console command to join a game, also used by the UI.
	virtual void Join(uint32 Index) final override;

	UFUNCTION(Exec)
	// Console command to refresh list of available servers.
	virtual void Refresh() final override;

protected:
	UFUNCTION(Exec)
	// Console command to host a game.
	// Works on the NULL Online Subsystem and for IPv4 Address server travel.
	virtual void Host() final override;

	UFUNCTION(Exec)
	// Console command to join a game using an IPv4 Address for client travel.
	virtual void JoinAt(const FString& Address) final override;
/// ~IMenuInterface

public:
	// The Main Menu's UClass* type
	TSubclassOf<UTL6MenuWidget> GetMainMenuWidgetClass() const;

	// The In-Game Menu's UClass* type
	TSubclassOf<UTL6MenuWidget> GetInGameMenuWidgetClass() const;

private:
	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess=true))
	TSubclassOf<UTL6MenuWidget> MainMenuWidgetClass;
	
	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess=true))
	TSubclassOf<UTL6MenuWidget> InGameMenuWidgetClass;

	/**
	 * The menu widget currently applicable.
	 * It's a MainMenuWidget if we're in the Main Menu Level.
	 * It's an InGameMenuWidget if we're in a Lobby or Playable Level.
	 */
	UPROPERTY()
	UTL6MenuWidget* MenuWidget;

	// The key which maps to the value containing the user-specified server name.
	const static FName KServerNameKey;

	UPROPERTY(Config)
	// Maximum search results permissible in a Find Sessions query. Config variable.
	uint32 MaxNumSearchResults;

	UPROPERTY(Config)
	// Time spent waiting for search results. Config variable.
	uint32 TimeoutInSeconds;

	// The Session Interface provided by the current Online Subsystem.
	IOnlineSessionPtr SessionInterface;

	// The settings used in a FindSessions() query, hoisted to ensure lifetime guarantee.
	TSharedPtr<FOnlineSessionSearch> FindSessionsSettings;

	FString ServerName;

	// Creates a public, advertised session for two players.
	void CreatePublicSession() const;

	// Callback fired when session finishes being created asynchronously.
	void OnCreateSessionComplete(FName SessionName, bool bSuccess) const;

	// Callback fired when session finishes being destroyed asynchronously.
	void OnDestroySessionComplete(FName SessionName, bool bSuccess) const;

	// Callback fired when a search for sessions via the Session Interface is complete.
	void OnFindSessionsComplete(bool bSuccess) const;

	// Callback fired when this instance finishes joining a session.
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result) const;

	// Behavior executed when a level starts loading.
	void BeginLoadingScreen(const FString& InMapName);
	
	// Behavior executed when a level ends loading.
	void EndLoadingScreen(UWorld* InLoadedWorld);

};

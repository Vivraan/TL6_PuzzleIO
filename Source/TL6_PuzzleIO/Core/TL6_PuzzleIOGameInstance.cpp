// Fill out your copyright notice in the Description page of Project Settings.

#include "TL6_PuzzleIOGameInstance.h"
#include "TL6_PuzzleIO.h"
#include "TL6MenuWidget.h"
#include "TL6MainMenuWidget.h"

#include "GameFramework/PlayerController.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Engine/Engine.h"
#include "Engine/World.h"
#include "MoviePlayer.h"


const FName UTL6_PuzzleIOGameInstance::KServerNameKey{TEXT("ServerNameKey")};


void UTL6_PuzzleIOGameInstance::Init()
{
	Super::Init();

	// Displays a simple loading widget while any level loads
	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UTL6_PuzzleIOGameInstance::BeginLoadingScreen);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &UTL6_PuzzleIOGameInstance::EndLoadingScreen);

	InitializeOnlineSessionInterface();
}

void UTL6_PuzzleIOGameInstance::InitializeOnlineSessionInterface()
{
	if (const IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get())
	{
		UE_LOG
		(
			LogTemp, Warning, TEXT("Found Online Subsystem: %s | Instance: %s"),
			*Subsystem->GetSubsystemName().ToString(),
			*Subsystem->GetInstanceName().ToString()
		);

		SessionInterface = Subsystem->GetSessionInterface();

		if (SessionInterface.IsValid())
		{
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(
				this, &UTL6_PuzzleIOGameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(
				this, &UTL6_PuzzleIOGameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(
				this, &UTL6_PuzzleIOGameInstance::OnFindSessionsComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(
				this, &UTL6_PuzzleIOGameInstance::OnJoinSessionComplete);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Online Subsystem found."));
	}
}

UTL6MenuWidget* UTL6_PuzzleIOGameInstance::LoadMenuWidget(const TSubclassOf<UTL6MenuWidget> MenuWidgetClass)
{
	MenuWidget = CreateWidget<UTL6MenuWidget>(this, MenuWidgetClass);

	if (MenuWidget)
	{
		MenuWidget->SetMenuInterface(this);
	}

	return MenuWidget;
}

void UTL6_PuzzleIOGameInstance::GotoMainMenuLevel()
{
	if (APlayerController* FirstLocalPlayerController = GetFirstLocalPlayerController())
	{
		// Take client to hosted level through level URL
		FirstLocalPlayerController->ClientTravel(TL6_PuzzleIOStatics::MainMenuLevel, ETravelType::TRAVEL_Absolute);
	}
}

void UTL6_PuzzleIOGameInstance::CreatePublicSession() const
{
	if (SessionInterface.IsValid())
	{
		FOnlineSessionSettings SessionSettings;

		SessionSettings.NumPublicConnections = 2;
		SessionSettings.bShouldAdvertise = true;

		/// If the SessionInterface is valid, so is the owning Online Subsystem
		SessionSettings.bIsLANMatch = IOnlineSubsystem::Get()->GetSubsystemName() == TEXT("NULL");

		/// Crucial for setting up an Internet session
		SessionSettings.bUsesPresence = true;

		SessionSettings.Set(KServerNameKey, ServerName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

		SessionInterface->CreateSession(0, NAME_GameSession, SessionSettings);
	}
}

void UTL6_PuzzleIOGameInstance::HostNamed(const FString& InServerName)
{
	if (SessionInterface.IsValid())
	{
		if (FNamedOnlineSession* ExistingSession = SessionInterface->GetNamedSession(NAME_GameSession))
		{
			SessionInterface->DestroySession(NAME_GameSession);
		}
		else
		{
			ServerName = InServerName;
			CreatePublicSession();
		}
	}
}

void UTL6_PuzzleIOGameInstance::Host()
{
	HostNamed(TEXT(""));
}

void UTL6_PuzzleIOGameInstance::OnCreateSessionComplete(FName SessionName, bool bSuccess) const
{
	if (!bSuccess)
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't create session \"%s\""), *SessionName.ToString());
		return;
	}

	UEngine* Engine = GetEngine();
	UWorld* World = GetWorld();

	if (Engine && World)
	{
		Engine->AddOnScreenDebugMessage(0, 5, FColor::Green, TEXT("Hosting..."));

		/// Take server to hosted level
		World->ServerTravel(TL6_PuzzleIOStatics::LobbyLevel + TEXT("?listen"));
	}
}

void UTL6_PuzzleIOGameInstance::OnDestroySessionComplete(FName SessionName, bool bSuccess) const
{
	/// If a session did exist and was destroyed, a new session replaces it
	if (bSuccess && SessionInterface.IsValid())
	{
		CreatePublicSession();
	}
}

void UTL6_PuzzleIOGameInstance::Join(uint32 Index)
{
	/// "Join" works only for the Main Menu
	const UTL6MainMenuWidget* MainMenu = Cast<UTL6MainMenuWidget>(MenuWidget);

	if (const bool bValid = MainMenu && SessionInterface.IsValid() && FindSessionsSettings.IsValid();
		bValid && FindSessionsSettings->SearchResults.IsValidIndex(Index))
	{
		const FOnlineSessionSearchResult& SelectedServer = FindSessionsSettings->SearchResults[Index];
		SessionInterface->JoinSession(0, NAME_GameSession, SelectedServer);
	}
}

void UTL6_PuzzleIOGameInstance::OnFindSessionsComplete(const bool bSuccess) const
{
	/// "Find Sessions" works only for the Main Menu

	if (UTL6MainMenuWidget* MainMenu = Cast<UTL6MainMenuWidget>(MenuWidget);
		bSuccess && MainMenu && FindSessionsSettings.IsValid())
	{
		FString LogString{TEXT("Find Sessions complete.\n")};
		const TArray<FOnlineSessionSearchResult>& Results{FindSessionsSettings->SearchResults};
		TArray<FOnlineSessionSearchResultStrings> ResultsStrings;

		// TODO: A manual switch for properly inserting test fields. Remove in future.
		//*/
		const uint32 NumTestFields = 0;
		/*/
		ResultsStrings.Insert(
			{
				{ TEXT("RIP"), TEXT("RIP"), TEXT("RIP"), TEXT("RIP") },
				{ TEXT("RIP"), TEXT("RIP"), TEXT("RIP"), TEXT("RIP") },
				{ TEXT("RIP"), TEXT("RIP"), TEXT("RIP"), TEXT("RIP") }
			},
			0
		);
		uint32 NumTestFields = ResultsStrings.Num();
		//*/

		if (FindSessionsSettings->SearchResults.Num() != 0)
		{
			LogString += TEXT("Found:\n");

			for (const FOnlineSessionSearchResult& Result : Results)
			{
				if (Result.IsValid())
				{
					// TODO: Relevant only for the NULL Subsystem. Remove in future.
					TArray<FString> NameTokens;
					Result.Session.OwningUserName.ParseIntoArray(NameTokens, TEXT("-"));

					FString ServerName;
					const bool bRetrievedServerName = Result.Session.SessionSettings.Get(KServerNameKey, ServerName);

					/// The NULL Subsystem may not display this correctly
					FString ConnectionsRatio =
						FString::Printf
						(
							TEXT("(%i/%i)"),
							Result.Session.SessionSettings.NumPublicConnections - Result.Session.
							NumOpenPublicConnections,
							Result.Session.SessionSettings.NumPublicConnections
						);

					ResultsStrings.Add
					(
						FOnlineSessionSearchResultStrings
						{
							bRetrievedServerName ? ServerName : Result.GetSessionIdStr(),
							FString::Printf(TEXT("%ims"), Result.PingInMs),
							NameTokens[0],
							ConnectionsRatio
						}
					);

					LogString +=
						FString::Printf
						(
							TEXT("%s | ping: %ims | %s | %s"),
							bRetrievedServerName ? *ServerName : *Result.GetSessionIdStr(),
							Result.PingInMs,
							*NameTokens[0],
							*ConnectionsRatio
						);
				}
			}
		}
		else
		{
			LogString += TEXT("No results found.");
		}

		MainMenu->PopulateServerList(ResultsStrings, NumTestFields);
		MainMenu->OnRefreshComplete();

		UE_LOG(LogTemp, Warning, TEXT("%s"), *LogString);
	}
}

void UTL6_PuzzleIOGameInstance::OnJoinSessionComplete(FName, EOnJoinSessionCompleteResult::Type Result) const
{
	if (Result == EOnJoinSessionCompleteResult::Success && SessionInterface.IsValid())
	{
		FString Address;
		UEngine* Engine{GetEngine()};
		APlayerController* FirstLocalPlayerController{GetFirstLocalPlayerController()};

		if (!SessionInterface->GetResolvedConnectString(NAME_GameSession, Address))
		{
			UE_LOG
			(
				LogTemp, Warning,
				TEXT("ConnectInfo could not be resolved from the Session named \"%s\"."),
				*FName{ NAME_GameSession }.ToString()
			)
		}
		else if (Engine && FirstLocalPlayerController)
		{
			const FString Message = TEXT("Joining address [") + Address + TEXT("]...");
			Engine->AddOnScreenDebugMessage(0, 5.f, FColor::Green, Message);

			FirstLocalPlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
		}
	}
}

void UTL6_PuzzleIOGameInstance::JoinAt(const FString& AddressString)
{
	UEngine* Engine{GetEngine()};
	APlayerController* FirstLocalPlayerController{GetFirstLocalPlayerController()};

	if (Engine && FirstLocalPlayerController)
	{
		FString Address{AddressString};
		if (TL6_PuzzleIOStatics::MakeValidIPv4Address(Address))
		{
			const FString Message = TEXT("Joining address [") + Address + TEXT("]...");
			Engine->AddOnScreenDebugMessage(0, 5.f, FColor::Green, Message);

			/// Take client to hosted level through IPv4 address
			FirstLocalPlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
		}
		else
		{
			Engine->AddOnScreenDebugMessage(0, 5.f, FColor::Red,
			                                TEXT("String entered is not a valid IPv4 address!"));
		}
	}
}

TSubclassOf<UTL6MenuWidget> UTL6_PuzzleIOGameInstance::GetMainMenuWidgetClass() const
{
	return MainMenuWidgetClass;
}

TSubclassOf<UTL6MenuWidget> UTL6_PuzzleIOGameInstance::GetInGameMenuWidgetClass() const
{
	return InGameMenuWidgetClass;
}

void UTL6_PuzzleIOGameInstance::Refresh()
{
	FindSessionsSettings = MakeShareable(new FOnlineSessionSearch);
	if (FindSessionsSettings.IsValid())
	{
		//FindSessionsSettings->bIsLanQuery = true;
		FindSessionsSettings->TimeoutInSeconds = TimeoutInSeconds;
		FindSessionsSettings->MaxSearchResults = MaxNumSearchResults;
		FindSessionsSettings->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

		UE_LOG(LogTemp, Warning, TEXT("Finding sessions..."));
		SessionInterface->FindSessions(0, FindSessionsSettings.ToSharedRef());
	}
}

// ReSharper disable once CppMemberFunctionMayBeStatic
void UTL6_PuzzleIOGameInstance::BeginLoadingScreen(const FString& InMapName)
{
	if (!IsRunningDedicatedServer())
	{
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = false;
		LoadingScreen.WidgetLoadingScreen = FLoadingScreenAttributes::NewTestLoadingScreenWidget();

		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
	}
}

// ReSharper disable once CppMemberFunctionMayBeStatic
void UTL6_PuzzleIOGameInstance::EndLoadingScreen(UWorld* InLoadedWorld)
{
	/// can be overriden to have custom effects
}

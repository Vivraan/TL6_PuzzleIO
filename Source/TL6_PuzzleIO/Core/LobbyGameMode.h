// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/TL6_PuzzleIOGameMode.h"
#include "LobbyGameMode.generated.h"

/**
 * This Game Mode is used specifically in the Lobby level.
 */
UCLASS()
class TL6_PUZZLEIO_API ALobbyGameMode : public ATL6_PuzzleIOGameMode
{
	GENERATED_BODY()
	
public:

	void PostLogin(APlayerController* NewPlayer) final override;
	
	void Logout(AController* Exiting) final	override;

private:

	uint32 NumPlayers;

	const static uint32 MAX_NUM_PLAYERS = 2U;
	
};

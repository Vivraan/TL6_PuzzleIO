// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TL6_PuzzleIO.h"
#include "Modules/ModuleManager.h"

#include "GameFramework/PlayerController.h"
#include "DefaultValueHelper.h"
#include "Engine/Engine.h"
#include "Engine/World.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TL6_PuzzleIO, "TL6_PuzzleIO" );
 

const FString TL6_PuzzleIOStatics::MainMenuLevel{ TEXT("/Game/PuzzleIO/Maps/MainMenu") };
const FString TL6_PuzzleIOStatics::LobbyLevel{ TEXT("/Game/PuzzleIO/Maps/Lobby") };
const FString TL6_PuzzleIOStatics::PlayableLevel{ TEXT("/Game/PuzzleIO/Maps/ThirdPersonExampleMap") };


void TL6_PuzzleIOStatics::Require(const UWorld* World, bool bCondition, const FString& ContextString)
{
	// if the condition is not met and World is defined, quit quietly - also works from editor
	if (!ensureMsgf((bCondition && World), TEXT("%s"), *ContextString))
	{
		// each player fires a quit message and exits from the game
		for (auto PlayerController = World->GetPlayerControllerIterator(); PlayerController; ++PlayerController)
		{
			PlayerController->Get()->ConsoleCommand("quit");
		}
	}
}

bool TL6_PuzzleIOStatics::MakeValidIPv4Address(FString& OutAddress)
{
	// Allow "localhost", but change the address to 127.0.0.1
	if (OutAddress.Contains(TEXT("localhost")))
	{
		OutAddress = TEXT("127.0.0.1");
		return true;
	}

	/// checks if the parsed string contains exactly 4 octets between 0 and 255
	TArray<FString> OutArray;

	int32 NumOctets = OutAddress.ParseIntoArray(OutArray, TEXT("."), false);
	int32 NumValidOctets = OutArray.FilterByPredicate(
		[](const FString& Component)
		{
			int32 ByteValue;
			bool bIsInt = FDefaultValueHelper::ParseInt(Component, ByteValue);
			bool bValidRange = FMath::IsWithinInclusive(ByteValue, 0, 255);
			return bIsInt && bValidRange;
		}
	).Num();

	return NumOctets == 4 && NumOctets == NumValidOctets;
}

void TL6_PuzzleIOStatics::PrintToScreenAndLogTemp(FString LogMessage, FColor LogColor)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *LogMessage);

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 5.F, LogColor, LogMessage);
	}
}

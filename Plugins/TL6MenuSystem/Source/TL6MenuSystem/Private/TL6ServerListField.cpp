// Copyright Vivraan (c)

#include "TL6ServerListField.h"
#include "TL6MainMenuWidget.h"


const FLinearColor UTL6ServerListField::KDefaultColor = FColor::White;
const FLinearColor UTL6ServerListField::KSelectedColor = FColor::Green;
const FLinearColor UTL6ServerListField::KHoveredColor = FColor::Yellow;


void UTL6ServerListField::Setup(UTL6MainMenuWidget* InParent, uint32 InIndex)
{
	if (FieldButton && InParent)
	{
		Parent = InParent;
		Index = InIndex;

		FieldButton->OnClicked.AddDynamic(this, &UTL6ServerListField::OnClicked);
		FieldButton->OnHovered.AddDynamic(this, &UTL6ServerListField::OnHovered);
		FieldButton->OnUnhovered.AddDynamic(this, &UTL6ServerListField::OnUnhovered);

		SetColorAndOpacity(KDefaultColor);
	}
}

void UTL6ServerListField::OnClicked()
{
	if (!bHeader)
	{
		Parent->SelectIndex(Index);

		bSelected = true;
		SetColorAndOpacity(KSelectedColor);
	}
}

void UTL6ServerListField::OnHovered()
{
	if (!(bHeader || bSelected))
	{
		SetColorAndOpacity(KHoveredColor);
	}
}

void UTL6ServerListField::OnUnhovered()
{
	if (!(bHeader || bSelected))
	{
		SetColorAndOpacity(KDefaultColor);
	}
}

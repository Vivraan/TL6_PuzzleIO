// Copyright Vivraan (c)

#include "TL6MenuWidget.h"


void UTL6MenuWidget::ShowWidget()
{
	AddToViewport();

	if (const UWorld* World = GetWorld())
	{
		if (APlayerController* PlayerController = World->GetFirstPlayerController())
		{
			FInputModeGameAndUI InputMode;
			InputMode.SetWidgetToFocus(TakeWidget());
			InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::LockOnCapture);

			PlayerController->SetInputMode(InputMode);
			PlayerController->bShowMouseCursor = true;
		}
	}

	bWidgetOnScreen = true;
}

void UTL6MenuWidget::HideWidget()
{
	if (const UWorld* World = GetWorld())
	{
		if (APlayerController* PlayerController = World->GetFirstPlayerController())
		{
			PlayerController->SetInputMode(FInputModeGameOnly());
			PlayerController->bShowMouseCursor = false;
		}
	}

	RemoveFromViewport();

	bWidgetOnScreen = false;
}

bool UTL6MenuWidget::IsWidgetOnScreen() const
{
	return bWidgetOnScreen;
}

void UTL6MenuWidget::SetMenuInterface(ITL6MenuInterface* Interface)
{
	MenuInterface = Interface;
}

void UTL6MenuWidget::OnLevelRemovedFromWorld(ULevel* Level, UWorld* World)
{
	HideWidget();

	Super::OnLevelRemovedFromWorld(Level, World);
}

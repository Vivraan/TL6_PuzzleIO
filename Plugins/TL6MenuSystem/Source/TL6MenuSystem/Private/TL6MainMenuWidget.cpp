// Copyright Vivraan (c)

// ReSharper disable CppMemberFunctionMayBeConst
#include "TL6MainMenuWidget.h"
#include "TL6MenuInterface.h"
#include "TL6MenuSystem.h"
#include "TL6ServerListField.h"

#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/TextBlock.h"
#include "Components/EditableTextBox.h"


bool UTL6MainMenuWidget::Initialize()
{
	/// Remember that Super::Initialize() must run first!
	if (!Super::Initialize()) return false;
	if (!IsValid(ServerListFieldClass)) return false;
		
	const bool bMainMenuValid = HostMenuButton && JoinMenuButton && ExitButton;
	const bool bHostMenuValid = HostGameButton && HostBackButton;
	const bool bJoinMenuValid = JoinGameButton && JoinBackButton && RefreshButton;

	if (bMainMenuValid)
	{
		HostMenuButton->OnClicked.AddDynamic(this, &UTL6MainMenuWidget::OpenHostMenu);
		JoinMenuButton->OnClicked.AddDynamic(this, &UTL6MainMenuWidget::OpenJoinMenu);
		ExitButton->OnClicked.AddDynamic(this, &UTL6MainMenuWidget::Exit);
	}
	else
	{
		return false;
	}

	if (bHostMenuValid)
	{
		HostGameButton->OnClicked.AddDynamic(this, &UTL6MainMenuWidget::HostServer);
		HostBackButton->OnClicked.AddDynamic(this, &UTL6MainMenuWidget::OpenMainMenu);
	}
	else
	{
		return false;
	}

	if (bJoinMenuValid)
	{
		JoinGameButton->OnClicked.AddDynamic(this, &UTL6MainMenuWidget::JoinServer);
		RefreshButton->OnClicked.AddDynamic(this, &UTL6MainMenuWidget::RefreshServerList);
		JoinBackButton->OnClicked.AddDynamic(this, &UTL6MainMenuWidget::OpenMainMenu);
	}
	else
	{
		return false;
	}

	return true;
}

void UTL6MainMenuWidget::PopulateServerList(const TArray<FOnlineSessionSearchResultStrings>& Results,
                                            const uint32 NumTestFields)
{
	if (UWorld* World = GetWorld())
	{
		ServerList->ClearChildren();

		/// Never add a ServerListField in the Widget editor, or its index will not be initialized!
		uint32 i = NumTestFields;
		for (const auto& [SessionIdStr, PingInMs, OwningUserName, OpenPublicConnectionsRatio] : Results)
		{
			if (auto* Field = CreateWidget<UTL6ServerListField>(World, ServerListFieldClass))
			{
				Field->PingInMs->SetText(FText::FromString(PingInMs));
				Field->SessionID->SetText(FText::FromString(SessionIdStr));
				Field->OwningUserName->SetText(FText::FromString(OwningUserName));
				Field->OpenPublicConnectionsRatio->SetText(FText::FromString(OpenPublicConnectionsRatio));
				Field->Setup(this, i++);

				ServerList->AddChild(Field);
			}
		}
	}
}

void UTL6MainMenuWidget::RefreshServerList()
{
	/// Very refreshing.
	static FText RefreshingText = FText::FromString(TEXT("Refreshing..."));

	if (MenuInterface && JoinMenu && RefreshButton)
	{
		Cast<UTextBlock>(RefreshButton->GetChildAt(0))->SetText(RefreshingText);
		MenuInterface->Refresh();
	}
}

void UTL6MainMenuWidget::OnRefreshComplete()
{
	static FText RefreshingText = FText::FromString(TEXT("Refresh"));

	if (RefreshButton)
	{
		Cast<UTextBlock>(RefreshButton->GetChildAt(0))->SetText(RefreshingText);
	}
}

void UTL6MainMenuWidget::SelectIndex(const uint32 Index)
{
	/// Change properties of previously selected field
	if (SelectedIndex.IsSet() && Index != SelectedIndex)
	{
		if (auto* Field = Cast<UTL6ServerListField>(ServerList->GetChildAt(SelectedIndex.GetValue())))
		{
			Field->bSelected = false;
			Field->SetColorAndOpacity(UTL6ServerListField::KDefaultColor);
		}
	}

	/// Then, change the index
	SelectedIndex = Index;
}

void UTL6MainMenuWidget::HostServer()
{
	if (MenuInterface && ServerNameField)
	{
		//MenuInterface->Host();

		MenuInterface->HostNamed(ServerNameField->GetText().ToString());
	}
}

void UTL6MainMenuWidget::JoinServer()
{
	if (MenuInterface && SelectedIndex.IsSet()/* && IPv4AddressField*/)
	{
		//MenuInterface->JoinAt(IPv4AddressField->GetText().ToString());

		UE_LOG(LogTemp, Warning, TEXT("Selected index %i."), SelectedIndex.GetValue());
		MenuInterface->Join(SelectedIndex.GetValue());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Selected index not set."));
	}
}

void UTL6MainMenuWidget::OpenHostMenu()
{
	if (MenuSwitcher && HostMenu)
	{
		MenuSwitcher->SetActiveWidget(HostMenu);
	}
}

void UTL6MainMenuWidget::OpenJoinMenu()
{
	if (MenuInterface && MenuSwitcher && JoinMenu)
	{
		MenuSwitcher->SetActiveWidget(JoinMenu);
		SelectedIndex.Reset();
		MenuInterface->Refresh();
	}
}

void UTL6MainMenuWidget::OpenMainMenu()
{
	if (MenuSwitcher && MainMenu)
	{
		MenuSwitcher->SetActiveWidget(MainMenu);
	}
}

void UTL6MainMenuWidget::Exit()
{
	if (const UWorld* World = GetWorld())
	{
		if (auto* FirstLocalPlayerController = World->GetFirstPlayerController())
		{
			FirstLocalPlayerController->ConsoleCommand(TEXT("quit"));
		}
	}
}

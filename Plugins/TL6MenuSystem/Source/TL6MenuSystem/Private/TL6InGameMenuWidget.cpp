// Copyright Vivraan (c)

#include "TL6InGameMenuWidget.h"
#include "TL6MenuInterface.h"

#include "Components/Button.h"


bool UTL6InGameMenuWidget::Initialize()
{
	if (Super::Initialize() && ResumeButton && ExitButton)
	{
		ResumeButton->OnClicked.AddDynamic(this, &UTL6InGameMenuWidget::ResumeGame);
		ExitButton->OnClicked.AddDynamic(this, &UTL6InGameMenuWidget::ExitToMainMenu);

		return true;
	}

	return false;
}

void UTL6InGameMenuWidget::ExitToMainMenu()
{
	if (MenuInterface)
	{
		HideWidget();
		MenuInterface->GotoMainMenuLevel();
	}
}

void UTL6InGameMenuWidget::ResumeGame()
{
	HideWidget();
}

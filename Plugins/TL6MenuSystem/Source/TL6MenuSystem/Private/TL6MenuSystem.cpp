// Copyright Epic Games, Inc. All Rights Reserved.

#include "TL6MenuSystem.h"

#include "DefaultValueHelper.h"

#define LOCTEXT_NAMESPACE "FTL6MenuSystemModule"

void FTL6MenuSystemModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
}

void FTL6MenuSystemModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FTL6MenuSystemModule, TL6MenuSystem)


const FString TL6Statics::KMainMenuLevel{ TEXT("/Game/PuzzleIO/Maps/MainMenu") };
const FString TL6Statics::KLobbyLevel{ TEXT("/Game/PuzzleIO/Maps/Lobby") };
const FString TL6Statics::KPlayableLevel{ TEXT("/Game/PuzzleIO/Maps/ThirdPersonExampleMap") };


void TL6Statics::Require(const UWorld* World, const bool bCondition, const FString& ContextString)
{
	// if the condition is not met and World is defined, quit quietly - also works from editor
	if (!ensureMsgf((bCondition && World), TEXT("%s"), *ContextString))
	{
		// each player fires a quit message and exits from the game
		for (auto PlayerController = World->GetPlayerControllerIterator(); PlayerController; ++PlayerController)
		{
			PlayerController->Get()->ConsoleCommand("quit");
		}
	}
}

bool TL6Statics::MakeValidIPv4Address(FString& OutAddress)
{
	// Allow "localhost", but change the address to 127.0.0.1
	if (OutAddress.Contains(TEXT("localhost")))
	{
		OutAddress = TEXT("127.0.0.1");
		return true;
	}

	/// checks if the parsed string contains exactly 4 octets between 0 and 255
	TArray<FString> OutArray;

	const int32 NumOctets = OutAddress.ParseIntoArray(OutArray, TEXT("."), false);
	const int32 NumValidOctets = OutArray.FilterByPredicate(
		[](const FString& Component)
		{
			int32 ByteValue;
			const bool bIsInt = FDefaultValueHelper::ParseInt(Component, ByteValue);
			const bool bValidRange = FMath::IsWithinInclusive(ByteValue, 0, 255);
			return bIsInt && bValidRange;
		}
	).Num();

	return NumOctets == 4 && NumOctets == NumValidOctets;
}

void TL6Statics::PrintToScreenAndLogTemp(const FString LogMessage, const FColor LogColor)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *LogMessage);

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 5.F, LogColor, LogMessage);
	}
}

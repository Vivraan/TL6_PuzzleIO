// Copyright Vivraan (c)

#pragma once

#include "CoreMinimal.h"
#include "TL6MenuWidget.h"
#include "TL6MainMenuWidget.generated.h"

class UButton;
class UWidgetSwitcher;
/**
 * The widget in charge of the game's main menu on the starting screen.
 */
UCLASS()
class TL6MENUSYSTEM_API UTL6MainMenuWidget : public UTL6MenuWidget
{
	GENERATED_BODY()
		
	friend class TL6ServerListField;

public:
	// TODO: Remove NumTestFields in the future.
	void PopulateServerList(const TArray<struct FOnlineSessionSearchResultStrings>& Results, uint32 NumTestFields = 0);

	UFUNCTION()
	// Initiates a server list refresh through a MenuInterface implementation.
	void RefreshServerList();

	// Called by a MenuInterface implementation whenever refreshing completes.
	void OnRefreshComplete();

	void SelectIndex(uint32 Index);

protected:

	bool Initialize() override;

	UPROPERTY(meta = (BindWidget))
	UWidgetSwitcher* MenuSwitcher;


	/// Main Menu
	UPROPERTY(meta = (BindWidget))
	UWidget* MainMenu;

	UPROPERTY(meta = (BindWidget))
	UButton* HostMenuButton;
	
	UPROPERTY(meta = (BindWidget))
	UButton* JoinMenuButton;

	UPROPERTY(meta = (BindWidget))
	UButton* ExitButton;
	/// ~Main Menu


	/// Host Menu
	UPROPERTY(meta = (BindWidget))
	UWidget* HostMenu;

	UPROPERTY(meta = (BindWidget))
	UButton* HostGameButton;

	UPROPERTY(meta = (BindWidget))
	UButton* HostBackButton;

	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* ServerNameField;
	/// ~Host Menu


	/// Join Menu
	UPROPERTY(meta = (BindWidget))
	UWidget* JoinMenu;

	UPROPERTY(meta = (BindWidget))
	UButton* JoinGameButton;

	UPROPERTY(meta = (BindWidget))
	UButton* RefreshButton;

	UPROPERTY(meta = (BindWidget))
	UButton* JoinBackButton;

	/// It's one of these two:
	UPROPERTY(meta = (BindWidget, OptionalWidget = true))
	UEditableTextBox* IPv4AddressField;

	UPROPERTY(meta = (BindWidget, OptionalWidget = true))
	UPanelWidget* ServerList;
	/// ~Join Menu

private:
	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess=true))
	TSubclassOf<UUserWidget> ServerListFieldClass;

	TOptional<uint32> SelectedIndex;

	UFUNCTION()
	void HostServer();

	UFUNCTION()
	void JoinServer();

	UFUNCTION()
	void OpenHostMenu();

	UFUNCTION()
	void OpenJoinMenu();

	UFUNCTION()
	void OpenMainMenu();

	UFUNCTION()
	void Exit();

};

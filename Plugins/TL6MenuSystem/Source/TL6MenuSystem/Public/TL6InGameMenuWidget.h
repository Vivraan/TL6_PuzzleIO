// Copyright Vivraan (c)

#pragma once

#include "CoreMinimal.h"
#include "TL6MenuWidget.h"
#include "TL6InGameMenuWidget.generated.h"

class UButton;
/**
 * 
 */
UCLASS()
class TL6MENUSYSTEM_API UTL6InGameMenuWidget : public UTL6MenuWidget
{
	GENERATED_BODY()

protected:

	bool Initialize() override;

	UPROPERTY(meta = (BindWidget))
	UButton* ResumeButton;
	
	UPROPERTY(meta = (BindWidget))
	UButton* ExitButton;

private:
	UFUNCTION()
	void ExitToMainMenu();

public:
	UFUNCTION()
	// Separating UFUNCTION binding from the HideWidget behaviour
	void ResumeGame();
};

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FTL6MenuSystemModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

// macros for TL6_PuzzleIO_Statics::Require, to ease off having to put in a few parameters
#define require(bCondition) TL6tatics::Require(GetWorld(), (bCondition), TEXT(""))

// macros for TL6_PuzzleIO_Statics::Require, to ease off having to put in a few parameters
#define requireMsg(bCondition, Context) TL6Statics::Require(GetWorld(), (bCondition), (Context))


struct TL6Statics final
{
	TL6Statics() = delete;

	// The three levels currently used in the game.
	static const FString KMainMenuLevel;
	static const FString KLobbyLevel;
	static const FString KPlayableLevel;

	// A goddarn assert with some real world value
	static void Require(const class UWorld* World, bool bCondition, const FString& ContextString);

	// A hacky IPv4 Address validator. Accepts "localhost": doing so modifies out parameter. 
	// Avoids including network module (for now).
	static bool MakeValidIPv4Address(FString& OutAddress);

	// Prints to log, and if possible, to the screen too!
	static void PrintToScreenAndLogTemp(FString LogMessage, FColor LogColor = FColor::Yellow);
};

/** A struct for holding the values from search results as formatted strings. Useful for debugging and lowering coupling. */
struct FOnlineSessionSearchResultStrings
{
	FString SessionIdStr;
	FString PingInMs;
	FString OwningUserName;
	FString OpenPublicConnectionsRatio;
};


// Copyright Vivraan (c)

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TL6MenuWidget.generated.h"

class ITL6MenuInterface;
/**
 * Encapsulates functionality such as showing and hiding a widget.
 */
UCLASS()
class TL6MENUSYSTEM_API UTL6MenuWidget : public UUserWidget
{
	GENERATED_BODY()


protected:
	// An interface object to minimise external dependencies.
	ITL6MenuInterface* MenuInterface;
	
	virtual void OnLevelRemovedFromWorld(ULevel* Level, UWorld* World) override;

	bool bWidgetOnScreen;

public:
	// Adds the Menu widget to viewport, and sets up other options, including the Input Mode.
	void ShowWidget();

	// Removes the Menu widget to viewport, and sets up other options, including the Input Mode.
	void HideWidget();

	// Verifies if the widget is on screen
	bool IsWidgetOnScreen() const;

	// Setter for MenuInterface
	void SetMenuInterface(ITL6MenuInterface* Interface);
};

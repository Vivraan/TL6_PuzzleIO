// Copyright Vivraan (c)

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TL6ServerListField.generated.h"

class UTextBlock;
/**
 * A field in a scroll box consisting of server details such as Session ID and ping.
 */
UCLASS()
class TL6MENUSYSTEM_API UTL6ServerListField : public UUserWidget
{
	GENERATED_BODY()
	
	friend class UTL6MainMenuWidget;

protected:
	UPROPERTY(meta = (BindWidget))
	UButton* FieldButton;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* SessionID;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* PingInMs;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* OwningUserName;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* OpenPublicConnectionsRatio;

	UPROPERTY(EditInstanceOnly, meta = (DisplayName = "Is a Header"))
	bool bHeader = false;

	UPROPERTY()
	UTL6MainMenuWidget* Parent;

	uint32 Index;

	bool bSelected = false;

	void Setup(UTL6MainMenuWidget* MainMenu, uint32 Index);

	static const FLinearColor KDefaultColor;

	static const FLinearColor KSelectedColor;

	static const FLinearColor KHoveredColor;

	UFUNCTION()
	void OnClicked();

	UFUNCTION()
	void OnHovered();

	UFUNCTION()
	void OnUnhovered();
	
};

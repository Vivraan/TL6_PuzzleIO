// Copyright Vivraan (c)

#pragma once

#include "CoreMinimal.h"
#include "TL6MenuInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTL6MenuInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * An interface for the Menu System to minimise project-specific dependencies.
 */
class TL6MENUSYSTEM_API ITL6MenuInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	// Creates a Menu widget from the supplied UClass.
	virtual void GotoMainMenuLevel() = 0;

	// Console command to host a game, also used by the UI.
	virtual void HostNamed(const FString& ServerName) = 0;

	// Console command to join a game, also used by the UI.
	virtual void Join(uint32 Index) = 0;
	
	// Console command to refresh list of available servers.
	virtual void Refresh() = 0;

protected:
	/** 
	 * Console command to host a game
	 * Works on the NULL Online Subsystem and for IPv4 Address server travel.
	 */
	virtual void Host() = 0;

	// Console command to join a game using an IPv4 Address for client travel.
	virtual void JoinAt(const FString& Address) = 0;

};
